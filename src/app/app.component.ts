import { Component } from "@angular/core";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent {
  //would need to be changed for pratical use eg dtabase conection to get newspaper data as mewspaper date will not always be todays date
  company = "NZ News";
  today: number = Date.now();
  title = "The Newspaper";
}
