import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FlexLayoutModule } from "@angular/flex-layout";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { SectionsComponent } from "./sections/sections.component";
import { SectionDetailComponent } from "./sections/section-detail/section-detail.component";
import { FrontPageComponent } from "./front-page/front-page.component";

@NgModule({
  declarations: [AppComponent, SectionsComponent, SectionDetailComponent, FrontPageComponent],
  imports: [BrowserModule, AppRoutingModule, FlexLayoutModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
