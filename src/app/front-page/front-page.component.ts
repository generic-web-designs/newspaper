import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-front-page",
  templateUrl: "./front-page.component.html",
  styleUrls: ["./front-page.component.scss"]
})
export class FrontPageComponent implements OnInit {
  FrontPage = {
    title: "Front Page News Story",
    content:
      "This story will show an exmaple on what a front page story could look like. To be fair most of the changes of the look of the content can be changed in the scss",
    size: "",
    image: {
      url: "assets/image/front.jpg",
      imageTitle: "Image Title",
      imageTitleSize: "",
      imageTitlePosition: "",
      imageSubtitle: "Image subtitle to give image alittle context if wished to",
      imageSubtitleSize: "",
      imageSubtitlePosition: ""
    },
    stories: [
      {
        title: "Front Page News Story",
        content:
          "This story will show an exmaple on what a front page story could look like. To be fair most of the changes of the look of the content can be changed in the scss",
        size: "",
        image: {
          url: "assets/image/front.jpg",
          imageTitle: "Image Title",
          imageTitleSize: "",
          imageTitlePosition: "",
          imageSubtitle: "Image subtitle to give image alittle context if wished to",
          imageSubtitleSize: "",
          imageSubtitlePosition: ""
        }
      }
    ],
    sidebarContent: [
      {
        title: "Featured Story Title",

        content: "An example sidebar could be a story in the newspaper",
        image: {
          url: "assets/image/front.jpg"
        }
      },
      {
        title: "Ad Title",
        content: "A different example can be ads from other companies",
        image: {
          url: "assets/image/front.jpg"
        }
      }
    ]
  };

  constructor() {}

  ngOnInit() {}
}
