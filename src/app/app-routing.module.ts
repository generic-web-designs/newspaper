import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { FrontPageComponent } from "./front-page/front-page.component";
import { SectionsComponent } from "./sections/sections.component";
import { SectionDetailComponent } from "./sections/section-detail/section-detail.component";

const routes: Routes = [
  { path: "", component: FrontPageComponent },
  { path: ":component", component: SectionsComponent },
  { path: ":component/:id", component: SectionDetailComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
